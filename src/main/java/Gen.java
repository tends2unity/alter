import java.util.Random;

/**
 * Created by soldatov on 12/28/16.
 */
public class Gen {
    private static final String[] NAMES = {"Парфен", "Трактор", "Акакий", "Гомодрал"};
    private Sample sample = new Sample();
    private boolean ageIsModify = false;
    private boolean manIsModify = false;
    private boolean nameIsModify = false;

    public Gen() {
    }

    public Gen(int age) {
        addSampleParam(age);
    }

    public Gen(String name) {
        addSampleParam(name);

    }

    public Gen(boolean isMan) {
        addSampleParam(isMan);
    }

    public Gen addSampleParam(Integer age) {
        sample.setAge(age);
        ageIsModify = true;
        return this;
    }

    public Gen addSampleParam(String name) {
        sample.setName(name);
        nameIsModify = true;
        return this;
    }

    public Gen addSampleParam(Boolean man) {
        sample.setMan(man);
        manIsModify = true;
        return this;
    }


    public Gen addSampleParam(Object param) throws Exception {
        if (param.getClass().equals(String.class)) {
            addSampleParam((String)param);
        } else if (param.getClass().equals(Boolean.class)) {
            addSampleParam((Boolean) param);
        } else if (param.getClass().equals(Integer.class)) {
            addSampleParam((Integer) param);
        } else {
            throw new Exception("Параметр типа : \""+param.getClass() + "\" не поддерживается");
        }
        return this;
    }

    public Sample getSample() {
        if (!ageIsModify) {
            sample.setAge(new Random().nextInt(99));
        }
        if (!manIsModify) {
            sample.setMan(new Random().nextBoolean());
        }
        if (!nameIsModify) {
            sample.setName(NAMES[new Random().nextInt(NAMES.length)]);
        }
        return sample;
    }
}
