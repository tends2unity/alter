/**
 * Created by soldatov on 12/28/16.
 *
 * Паттерны проектирования. Есть неизменяемый класс Sample (его можно только использовать, но не изменять!):
 *
 */
public class Sample {
    private String name;
    private int age;
    private boolean man;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isMan() {
        return man;
    }

    public void setMan(boolean man) {
        this.man = man;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
