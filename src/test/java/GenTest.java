import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by soldatov on 12/28/16.
 */
public class GenTest {
//    static List<Object> args = new ArrayList();

    static Gen gen = new Gen();

    @Test
    public void testGetSampleType() throws Exception {
        Assert.assertEquals(gen.getSample().getClass(), Sample.class);
    }


    @Test
    public void testGetSampleName() throws Exception {
        gen.addSampleParam("Name");
        Assert.assertEquals(gen.getSample().getName(), "Name");

    }

    @Test
    public void testGetSampleAge() throws Exception {
        gen.addSampleParam(123);
        Assert.assertEquals(gen.getSample().getAge(), 123);
    }

    @Test
    public void testGetSampleNameAge() throws Exception {
        gen.addSampleParam("Name");
        Assert.assertEquals(gen.getSample().getName(), "Name");
        gen.addSampleParam(123);
        Assert.assertEquals(gen.getSample().getAge(), 123);
    }

    @Test
    public void testGetSampleNameAgeMan() throws Exception {
        gen.addSampleParam("Name");
        Assert.assertEquals(gen.getSample().getName(), "Name");
        gen.addSampleParam(123);
        Assert.assertEquals(gen.getSample().getAge(), 123);
        gen.addSampleParam(true);
        Assert.assertEquals(gen.getSample().isMan(), true);
    }



}


