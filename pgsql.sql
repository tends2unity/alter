create table A (ID integer,Name character varying (255),City character varying (255),Salary integer );
INSERT INTO A VALUES ( 1,'Ivanov','MSK',100000);
INSERT INTO A VALUES ( 2,'Luzhkov','SPB',90000);
INSERT INTO A VALUES ( 3,'Sidorov','SPB',60000);
INSERT INTO A VALUES ( 4,'Kozlov','MSK',105000);
INSERT INTO A VALUES ( 5,'Komarov','SAM',60000);
INSERT INTO A VALUES ( 6,'Peshkov','SPB',80000);
INSERT INTO A VALUES ( 7,'Nilov','SAM',45000);
INSERT INTO A VALUES ( 8,'Krivoruchko','MSK',105000);
INSERT INTO A VALUES ( 9,'Krasnosheev','MSK',90000);



-- 1.
SELECT ID, NAME, CITY, SALARY, rank() OVER (PARTITION BY CITY ORDER by name asc) FROM A;

--2.
SELECT a.ID, a.NAME, a.CITY,a.SALARY FROM A a where a.salary = (select max(salary) from A b where a.CITY = b.CITY) order by city;